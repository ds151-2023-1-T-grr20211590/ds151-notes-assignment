import React, { createContext, useReducer } from 'react';
import annotations from '../data/annotations';

const initialState = { annotations };
const AnnotationsContext = createContext({});

const reducers = {
    createAnnotation(state, action) {
      const annotation = action.payload
      annotation.id = Math.random()
      return {
        ...state,
        annotations: [...state.annotations, annotation]
      }
    },

    updateAnnotation(state, action) {
      const updated = action.payload
      return {
        ...state,
        annotations: state.annotations.map(a => a.id === updated.id ? updated : a)
      }
    },

    deleteAnnotation(state, action) {
      const annotation = action.payload
        return {
          ...state,
          annotations: state.annotations.filter(a => a.id !== annotation.id)
        }
    }
}

export const AnnotationsProvider = (props) => {
    function reducer(state, action) {
      const fn = reducers[action.type]
      return fn ? fn(state, action) : state
    }
    
    const [state, dispatch] = useReducer(reducer, initialState)

    return (
      <AnnotationsContext.Provider value={{ state, dispatch }}>
        {props.children}
      </AnnotationsContext.Provider>
    )
} 

export default AnnotationsContext;