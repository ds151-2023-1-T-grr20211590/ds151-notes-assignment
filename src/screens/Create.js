import React, { useContext, useState } from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';
import AnnotationsContext from '../context/AnnotationContext';

const Create = ({ route, navigation }) => {
  const [annotation, setAnnotation] = useState(route.params ? route.params : {});
  const { dispatch } = useContext(AnnotationsContext);

  return(
    <View style={styles.container}>
      <Text>Anotação</Text>
      <TextInput 
        style={styles.input}
        onChangeText={name => setAnnotation({...annotation, name })} 
        placeholder="Nome da anotação"
        value={annotation.name}
      />
    <Text>Descrição</Text>
    <TextInput 
      style={styles.input}
      onChangeText={description => setAnnotation({...annotation, description})} 
      placeholder="Descrição da anotação"
      value={annotation.description}
    />
    <Button 
      title="Criar"
      onPress={() => {
        dispatch({
          type: annotation.id ? 'updateAnnotation' : 'createAnnotation',
          payload: annotation
        })
        navigation.goBack()
      }}
    />
  </View>
  )
}

const styles = StyleSheet.create({
    container: {
        padding: 12
    },

    input: {
        height: 40,
        borderColor: 'black',
        borderWidth: 1,
        marginBotton: 10
    }
})

export default Create;
