import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import List from './src/screens/List';
import Create from './src/screens/Create';
import { AnnotationsProvider } from './src/context/AnnotationContext';

const Stack = createStackNavigator();

export default function App() {
  return (
    <AnnotationsProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='List'>
          <Stack.Screen 
            name="List" 
            component={List} 
            options={({ navigation }) => {
              return {
                title: "Lista de anotações",
                headerRight: () => (
                  <Button 
                    onPress={() => navigation.navigate("Create")}
                    type="clear"
                    icon={<Icon name="add" size={25} color="black" />}
                  />
                )
              }
            }}  
          />

          <Stack.Screen name="Create" component={Create} options={{ title: "Criar anotações"}}/>
        </Stack.Navigator>
      </NavigationContainer>
    </AnnotationsProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
