import React, { useContext } from 'react';
import { Text, View, FlatList, Alert } from 'react-native';
import { Button, Icon, ListItem } from 'react-native-elements';

import AnnotationsContext from '../context/AnnotationContext';

const List = (props) => {
  const { state, dispatch } = useContext(AnnotationsContext);
    
  function confirmAnnotationDelete( annotation ) {
    Alert.alert('Excluir anotação', 'Deseja excluir anotação?', [
      {
        text: 'Sim',
        onPress() {
          dispatch({
            type: 'deleteAnnotation',
            payload: annotation
          })
        }
      },
      {
        text: 'Não'
      },
    ])
  }

  function getActions( annotation ) {
    return (
      <>
        <Button 
          onPress={() => props.navigation.navigate('Create', annotation)}
          type="clear"
          icon={<Icon name="edit" size={25} color="orange" />} 
        />
        <Button 
          onPress={() =>  confirmAnnotationDelete(annotation)}
          type="clear"
          icon={<Icon name="delete" size={25} color="red" />} 
        />
      </>
    )
  }

  function getAnnotationItem({ item : annotation }) {
    return (
      <>
      <ListItem 
      key={annotation.id} 
      bottonDivider
      onPress={() => props.navigation.navigate('Create', annotation)}
      >
        <ListItem.Content>
          <ListItem.Title>{annotation.name}</ListItem.Title>
          <ListItem.Subtitle>{annotation.description}</ListItem.Subtitle>
        </ListItem.Content>
        <Button 
          onPress={() => props.navigation.navigate('Create', annotation)}
          type="clear"
          icon={<Icon name="edit" size={25} color="orange" />} 
        />
        <Button 
          onPress={() =>  confirmAnnotationDelete(annotation)}
          type="clear"
          icon={<Icon name="delete" size={25} color="red" />} 
        />
      </ListItem>
      </>
    )
  }

  return(
    <View>
      <FlatList
        keyExtractor={annotation => annotation.id.toString()}
        data={state.annotations}
        renderItem={getAnnotationItem}
      />
    </View>
  )
}

export default List;
